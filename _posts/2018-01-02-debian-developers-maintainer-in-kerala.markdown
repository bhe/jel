---
title:  "Debian {Developers, Maintainers} in Kerala"
date:   2018-01-02 14:09:23 +530
comments: true
categories: [debian]
tags: [debian]
kerala: https://en.wikipedia.org/wiki/Kerala
balasankarc: https://balasankarc.in
abhijith: https://abhijithpa.me
sruthi: https://poddery.com/people/258b5aa02c41013359e47e8f7b917a82 
praveen: https://poddery.com/people/45fa8bea21b8a0f5
---

We have three Debian Developers and two Debian Maintainers here in <a href="{{page.kerala}}">Kerala</a>.


Debian Developers 


* <a href="{{page.balasankarc}}"> Balasankar C </a> (balasankarc) 
* <a href="{{page.praveen}}"> Praveen Arimbrathodiyil</a> (j4v4m4n) 
*  Ramakrishnan MuthuKrishnan 

Debian Maintainers


* me :) <a href="{{page.abhijith}}"> Abhijith PA </a> (bhe)   
* <a href="{{page.sruthi}}"> Sruthi Chandran </a> (srud)  

I haven't met Ramakrishnan Muthukrishnan yet and not sure whether he is still in Kerala. But I know rest of them. We always meet on almost all FOSS conferences in Kerala.  
