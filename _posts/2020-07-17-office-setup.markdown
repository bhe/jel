---
title: "Workstation setup"
date: 2020-07-16 10:07:00 +530
comments: true
categories: [hardware]
tags: [misc]
samsung: https://www.samsung.com/levant/support/model/NP-R439-DA02JO/
fomo: https://en.wikipedia.org/wiki/Fear_of_missing_out
earlier: https://abhijithpa.me/2018/My-Laptop/
stand: https://www.amkette.com/product/it-peripherals/amkette-ergo-view-laptop-stand-with-7-adjustment-levels-for-laptops-up-to-17-inches-12-inch13-inch-14-1-inch15-6-inch17-inch-grey/
dell: https://www.dell.com/en-in/shop/dell-optical-mouse-ms116-black/apd/570-aajg/pc-accessories
keyboard: https://www.fingers.co.in/computer-peripherals/backlit-keyboards
monitor: https://www.micromaxinfo.com/led-monitors
monitor2: https://www.viewsonic.com/au/products/lcd/VA1918wm.php
headphone: https://www.boat-lifestyle.com/collections/earphones/products/bassheads-220
headphone2: https://www.flipkart.com/mi-basic-wired-headset-mic/p/itmdd96364a6f286?pid=ACCF3YPVECGQXTKK&lid=LSTACCF3YPVECGQXTKKSQ7SA7&marketplace=FLIPKART&srno=s_1_2&otracker=search&otracker1=search&fm=SEARCH&iid=5ec38ce4-147e-475b-b51d-7413df463b04.ACCF3YPVECGQXTKK.SEARCH&ppt=sp&ppn=sp&ssid=q9820rjw4sn2imf41594906774879&qH=d22c6e903708fda3
---
Hello,

Recently I've seen lot of people sharing about their home office
setup. I thought why don't I do something similar. Not to beat <a
href="{{page.fomo}}">FOMO</a>,
but in future when I revisit this blog, it will be lovely to
understand that I had some cool stuffs.


There are people who went deep down in the ocean to lay cables for me
to have a remote job and I am thankful to them.


Being remote my home is my office. On my work table I have a <a
href="{{page.samsung}}">Samsung R439</a> laptop. I've blogged about it
<a href="{{page.earlier}}">earlier</a>. New addition is that it
have another 4GB RAM, a total of 6GB and 120GB SSD. I run Debian
testing on it. Laptop is placed on a <a
href="{{page.stand}}">stand</a>. <a href="{{page.dell}}">Dell MS116</a> as external mouse always
connected to it. I also use an external keyboard from <a
href="{{page.keyboard}}">fingers</a>. Its keys are very stiff and
I don't recommend to anyone. The only reason I took this keyboard that it is in my budget and have a backlit,
which I needed most.


I have a Micromax <a href="{{page.monitor}}">MM215FH76</a> 21 inch monitor as my secondary display which stacked up on
couple of old books to adjust the height with laptop stand. Everything is ok with this
monitor except that it don't have HDMI point and stand is very
weak. I use i3wm and this small script help me to manage my monitor
arrangement.

```
# samsung r439
xrandr --output LVDS1 --primary --mode 1366x768 --pos 1920x312 --rotate normal --output DP1 --off --output HDMI1 --off --output VGA1 --mode 1920x1080 --pos 0x0 --rotate normal --output VIRTUAL1 --off
# thinkpad t430s
#xrandr --output LVDS1 --primary --mode 1600x900 --pos 1920x180 --rotate normal --output DP1 --off --output DP2 --off --output DP3 --off --output HDMI1 --off --output HDMI2 --off --output HDMI3 --off --output VGA1 --mode 1920x1080 --pos 0x0 --rotate normal --output VIRTUAL1 --off
i3-msg workspace 2, move workspace to left
i3-msg workspace 4, move workspace to left
i3-msg workspace 6, move workspace to left

```


I also have another <a href="{{page.monitor2}}">Viewsonic</a> monitor 19 inch, it started to
show some lines and unpleasant colors. Thus moved back to shelf.

I have an orange pi zero plus 2 running Armbian which serve as my emby
media server. I don't own any webcam or quality headset at the moment.
I have a <a href="{{page.headphone}}">boat</a>, and <a
href="{{page.headphone2}}">Mi</a>, headphones. My laptop inbuilt webcam is
horrible, so for my video conferencing need I use jitsi app on my mobile device.


