---
title: "FOSSASIA experience"
date: 2018-04-5 12:24:00 +530
comments: true
categories: [debian, conference,travel]
tags: [debian, conference, travel]
fossasia: https://2018.fossasia.org
wiki: https://wiki.debian.org/FOSSASIA/meet_at_venue
---

Hello Everyone !

I was able to attend this years <a href="{{page.fossasia}}">FOSSASIA</a> summit held at Lifelong Learning Institute,
 Singapore. Its a quite decent sized, 4 day long conference filled with lot of
parallel tracks that covers vast areas from fun tinkering things to huge block
chain and data mining topics (which I consider as big topic). Way too much 
parallel tracks that I decided to attend less talk and meet more people around 
the venue. The atmosphere was very hacker friendly.

![Debian Booth](/images/abhijith_fossasia.png)

I spend most of my time at the Debian booth. People swing by booth and
they talked about their experience with Debian. It was fun to
meet them all. Prior to the conference I created a <a href="{{page.wiki}}">wiki page</a> to coordinate Debian booth at exhibition which really helped.   

I met three Debian Developers - Chow Loong Jin (hyperair), Andrew Lee 李健秋 (ajqlee) and Héctor Orón Martínez (zumbi). Andrew Lee and zumbi also volunteered at Debian booth from time to time along with
Balasankar 'balu' C (balasankarc). Hyperair was sitting at HackerspaceSG booth, just two booth across from us.

![group photo](/images/debian-booth_fossasia.png)

All in all it was an amazing conference. I want to reach out to the
organizers and thank them for FOSSASIA.


#### Singapore


Singapore is a beautiful city with lots of tourists and tourist
attractions. All places are well connected with public transport system.
People can reach every corner of Singapore with Metro trains
and buses. you can find huge variety of food in Singapore.
Stalls serving light meals and restaurants are everywhere. On top
of that you can find stores like 7-eleven where you can get instant
noodles and similar stuffs. Anish (a Debian contributor, also friend of me and 
balu from kerala who now lives in Singapore) taught me how to use chopsticks :). I
also brought home a pair of chopsticks as souvenir that came with my lunch.


![group photo](/images/balu_fossasia.png)
![group photo](/images/andrew_fossasia.png)
![group photo](/images/zumbi_fossasia.png)