---
title: "DebUtsav Delhi"
date: 2019-04-02 13:54:00 +530
comments: true
categories: [debian, conference, travel]
tags: [debian, conference, travel]
debutsav: https://debutsav.in/
free: https://www.linuxdelhi.org/
election: https://pirates.org.in/elections/loksabha2019
lok: https://en.wikipedia.org/wiki/2019_Indian_general_election
srud: https://salsa.debian.org/srud-guest
isaagar: https://wiki.debian.org/I%20Sagar
hamara: https://hamaralinux.org
---
Hello.


Three weeks ago I was able to attend <a href="{{page.debutsav}}">DebUtsav-Delhi</a> organized by the Debian and <a href="{{page.free}}">free</a> folks in North India. 

[Group photo](/images/debutsav-del.png)

Debutsav-Delhi is the third edition of its kind.
Initially Mozilla Delhi backed the Debutsav-delhi when
they pitched the idea but later they withdrew for some reason and just became a supporting member. I must say Debian India events are happening frequent now. Some years ago in India Debian hang around with other FLOSS events. Now its DebUtsav giving chance to other FLOSS people to meet around Debian.


As the usual way of DebUtsav, this one also was two day event with
separate track for Debian related talks and for general FLOSS talk. I gave a talk about Debian LTS project. On first day evening some speakers and organizers gathered for dinner.

[Dinner](/images/dinner.png)

Its funny that most of the Debian people gathered
there were contributing/contributed to Ruby and JavaScript team . There
is a strong reason for that. All the contributors to Debian from India
after 2014 were branched out from a single person who do mostly Ruby and
JS - Pirate Praveen. You can expect a blog post from him about Debutsav. He is <a href="{{page.election}}">contesting</a> in upcoming <a href="{{page.lok}}">Lok Sabha Elections</a> and quite busy with that.

On second day there were talks from SFLC - Digital Security and Privacy. <a href="{{page.srud}}">Srud</a> conducted a interactive
session with topic Gender diversity in FLOSS projects. We reserved
afternoon sessions for Bug Squashing Party and introducing packaging
tutorial to newcomers. All together it was a wonderful gathering. I also met <a href="{{page.isaagar}}">isaagar</a> whom with I have corresponded in matrix a lot but finally able to meet him IRL. 

Special appreciation to <a href="{{page.hamara}}">Hamara Linux</a> for sponsoring the event.They are
becoming the de facto sponsors of every Debian events in India.
