---
title:  "Attending Pyconf Hyderabad"
date:   2018-01-01 23:16:23 +530
comments: true
categories: [Conference, Travel]
tags: [Conference]
icfoss: https://icfoss.in/
community: https://pyconf.hydpy.org/
iiit: https://www.iiit.ac.in/
linaro: https://www.linaro.org/
separation: https://www.quora.com/Why-was-the-state-of-Andhra-Pradesh-split-in-two
---
Chris Lamb if you are seeing this, then I am on Debian Planet :) !!!


![Hyderabad Pyconf](/images/pyconf.jpg){:height="400px" width="500px"}
Even though I mostly hack on Ruby gems in my Debian time, Python is my first choice of programming language. I maintain a Python archiving application in Debian. 


Apart from a local meet-up I never attended any Python social gathering. I always wanted to attend Pycons because whenever I search something related to python on Youtube I get talks from various Pycons. So I was looking for a Pycon in India and I found out Hyderabad <a href="{{page.community}}">community</a> is organizing one in <a href="{{page.iiit}}">IIIT</a> campus which is on a perfect weekend for me. I planned the itineraries and found out the travel and accommodation is over my budget. I did the next obvious thing that everyone do which is asking sponsorship from the host but with my Debian hat on. They told they can’t help. Then I asked <a href="{{page.icfoss}}">ICFOSS</a> which is an autonomous organization created by Kerala Government to foster the Free Software activities in the state. Their answer was also same. They didn’t even have any program to support Free software developers yet. My last hope was Debian. I sent grant request to our DPL Chris Lamb. He was very kind enough to approve the request on the same day itself :). \o/ .


Every thing set. Fast forward to 8th October morning. I woke up around 5 AM. Bad weather, it was a very rainy day. But somehow I caught bus to nearest town and from there I took uber cab to the Airport. Checked in and entered to aeroplane. It was an airbus A-320 aircraft. 
After Landing in Rajeev Gandhi International Airport, Hyderabad. I quickly jumped in to Ola cab to Gachibowli where the event is happening. Quick to registration desk collected my tags and goodies. The talks were already began. During break I caught up with Ramanathan and then Manivannan. I had contact with these people prior the event as they helped in answering a lot of my doubts. There were couple of stalls in the venue by Python Software Foundation, Redhat, Pramati, IBM. I went to PSF stall and collected some stickers. I also had some Debian stickers with me. I put it in the PSF desk and people start swarming to get it, which is very cool to see. I should have put it on the Redhat desk :). Then I lurked around other stalls. From Redhat stall I met Sourabh, he is also a Fedora contributor. We had a good chat about Debian and fedora. He also showed his workstation which is a Thinkpad T470s. And during lunch break I met Naresh, he works for <a href="{{page.linaro}}">Linaro</a>.

![Hyderabad Pyconf](/images/stickers.jpg){:height="500px" width="500px"}

The evening sessions were wonderful. There were talks about serverless IoTs and about Stephanie which is a Virtual assistant creating platform.
Then the last keynote was from Kushal Das. He is from Python Software Foundation and he is also associated with projects like Digital Freedom Press, Fedora. Kushal talked about the hack cultures, first generation hackers. This was very exiting talk. After session I went to meet Kushal and we talked about Tor and Dmitry Bogatov who got arrested for running Tor exit node. 


After the event I left the IIIT campus and walked outside. When I started the journey I have plans to see the city a little bit, but night already consumed the streets. I decided  on one thing. Instead of taking cab lets travel on bus. I took a city bus to the airport. It was a Telengana State Road Transport Corporation (TSRTC) bus which is similar to Kerala’s KSRTC expect that it has a name called “Pushpak”. After one hour I reached at airport.  I got plenty of time to catch my flight to home. So I just wandered around the airport to kill time. When I landed here in the morning, I didnt even look around. Now I got some time to see it. After that I was just curious about the  politics in Andhra Pradhesh- Telengana <a href="{{page.separation}}">separation</a>. So just read some online articles about the issues. 
I entered to the airport and I still have time. So I took a small nap. After that I went to collect boarding pass and moved to aircraft. I took another nap in the plane. I woke up when the air hostess called me to revert the push back seat. So basically we were about to touch down in matter of minutes. After landing in the CIAL( Cochin International Airport Limited ). I got the bus to my home. It was a good travel after all. Only mistake I made is when I collected my T-shirt I forgot to check its size and after reaching home I found out it was small. 
